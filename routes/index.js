var express = require('express');
var Wave  = require('../models/wave');
var router = express.Router();

/* GET home page. */
router.get('/:time/:amplitude', function(req, res, next) {
  time=req.params.time,
  amplitude=req.params.amplitude
Wave.updatetime(time,(err,wave)=>{
  if(err){
    throw err;
  }
  else{
    Wave.updateamplitude(amplitude,(err,wave)=>{
      if(err){
        throw err;
      }
      else{
        res.send("time and amplitude added");
      }
    })
  }
})

});

router.get('/create',(req,res,next)=>{
  newwave = new Wave({
    time:0,
    amplitude:0,
    date:new Date
  })
  Wave.createWave(newwave,(err,wave)=>{
    if(err){
      throw err;
    }
    else{
      res.send("created");
    }
  });
})


module.exports = router;
