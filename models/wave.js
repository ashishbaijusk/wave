const mongoose=require('mongoose');

const Schema=mongoose.Schema;
mongoose.Promise=global.Promise;

const waveschema =   new Schema({
  date:Date,
   time:[],
   amplitude:[]
});

const   Wave    =   module.exports  =   mongoose.model("Wave",waveschema);
module.exports.createWave =   (newwave,callback)=>{
    Wave.create(newwave, callback);
  }

  module.exports.updatetime = (time,callback)=>{
    Wave.findOne({},(err,wave)=>{
        if(err){
            throw   err;
        }
        else{
          id=wave._id
            Wave.findOneAndUpdate({"_id":id}, {"$push":{ "time": time }},callback);
          }
      });
        }
        module.exports.updateamplitude = (amplitude,callback)=>{
          Wave.findOne({},(err,wave)=>{
              if(err){
                  throw   err;
              }
              else{
                id=wave._id
                  Wave.findOneAndUpdate({"_id":id},{"$push":{"amplitude":amplitude}},callback);
                }
            });
              }
